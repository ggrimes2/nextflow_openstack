# nextflow_openstack

Extra configuration need for OpenStack Nextflow VM to run Nextflow workshop

## Configure conda

Add conda channels to `.condarc`

~~~
conda config --add channels bioconda
conda config --add channels conda-forge
~~~

## Install Nextflow in conda base environment

~~~
conda install nextflow -n base
~~~

## Download workshop repository


~~~
git clone https://github.com/ggrimes/workflows-nextflow
~~~

## Install conda environment from file 

Install nf-core/hlatyping environment to speed up workshop

~~~
wget https://raw.githubusercontent.com/nf-core/hlatyping/master/environment.yml
conda env create -f environment.yml
~~~

## Download nextflow.config

Download nextflow.config in /home/training

~~~
wget https://git.ecdf.ed.ac.uk/ggrimes2/nextflow_openstack/-/raw/main/nextflow.config
~~~
